import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Ryan on 4/18/2016.
 */
public class Lol {



    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private static final File HOME_DIRECTORY = new File("").getAbsoluteFile();

    public static void main(String[] args) throws IOException {
        ArrayList<Snowboard> boardList = loadBoards("/catalog");
        Person evan = new Person();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your snowboarding skill level.");
        System.out.println("1 - Newbie");
        System.out.println("2 - Amateur");
        System.out.println("3 - Novice");
        System.out.println("4 - Advanced");
        System.out.println("5 - Professional");
        int skill = 0;
        do {
            System.out.print("Skill: ");
            if (scan.hasNextInt()) {
                if ((skill = scan.nextInt()) < 1 || skill > 5)
                    skill = 0;
            } else scan.next();
        } while (skill == 0);
        evan.setSkill(skill);
        System.out.println("What is your sex (male or female)?");
        String sex = null;
        do {
            System.out.print("Sex: ");
            String response = scan.next();
            if (response.equals("male") || response.equals("female"))
                sex = response;
        } while (sex == null);
        evan.setMale(sex.equals("male"));

        System.out.println("Enter your preferred terrain riding type");
        System.out.println("1 - Park/Pipe riding");
        System.out.println("2 - Groomer trails");
        System.out.println("3 - Back-country Riding");
        int terrain = 0;
        do {
            System.out.print("Terrain: ");
            if (scan.hasNextInt()) {
                if ((terrain = scan.nextInt()) < 1 || terrain > 3)
                    terrain = 0;
            } else scan.next();
        } while (terrain == 0);
        evan.setTerrain(TerrainType.values()[terrain - 1]);

        System.out.println("Enter your max budget");
        float budget = 0;
        do {
            System.out.print("Budget: ");
            if (scan.hasNextFloat())
                budget = scan.nextFloat();
            else scan.next();
        } while (budget <= 0);
        evan.setBudget(budget);

        System.out.println("Enter your weight in whole pounds");
        int weight = 0;
        do {
            System.out.print("Weight: ");
            if (scan.hasNextInt())
                weight = scan.nextInt();
            else scan.next();
        } while (weight <= 0);
        evan.setWeight(weight);

        System.out.println("Enter your height in whole inches");
        int height = 0;
        do {
            System.out.print("Height: ");
            if (scan.hasNextInt())
                height = scan.nextInt();
            else scan.next();
        } while (height <= 0);
        height *= 2.54;
        evan.setHeight(height);

        System.out.println("Enter your age");
        int age = 0;
        do {
            System.out.print("Age: ");
            if (scan.hasNextInt())
                age = scan.nextInt();
            else scan.next();
        } while (age <= 0);
        evan.setAge(age);

        System.out.println("Enter your shoe size");
        float shoe = 0;
        do {
            System.out.print("Size: ");
            if (scan.hasNextInt())
                shoe = scan.nextInt();
            else scan.next();
        } while (shoe <= 0);
        if (!evan.isMale())
            shoe -= 1.5;
        evan.setShoe(shoe);

        boardList.sort(evan);
        for (int i = 0; i < 5; i++) {
            Snowboard b = boardList.get(i);
            System.out.printf("[%3d] %s (%s)\n", evan.score(b), b, b.getLink());
        }
    }

    public static ArrayList<Snowboard> loadBoards(String dir) throws IOException {
        ArrayList<Snowboard> results = new ArrayList<>();
        File[] files = new File(HOME_DIRECTORY, "/catalog").listFiles();
        if (files != null) {
            for (File f : files) {
                Snowboard board = null;
                try (FileReader reader = new FileReader(f)) {
                    board = GSON.fromJson(reader, Snowboard.class);
                } catch (Exception whatever) {
                    throw new IOException("Error parsing " + f.getAbsolutePath(), whatever);
                }
                if (board != null)
                    results.add(board);
            }
        }
        return results;
    }

}
