/**
 * Created by Ryan on 4/18/2016.
 */
public class Snowboard {

    private String board;
    private String brand;
    private float[] lengths;
    private float[] wide;
    private String camber;
    private boolean male;
    private boolean symmetry;
    private TerrainType[] terrain;
    private float price;
    private int flex;
    private Shape shape;
    private String link;



    public Snowboard() {
        board = "";
        brand = "";
        lengths = new float[0];
        wide = new float[0];
        camber = "";
        male = true;
        symmetry = true;
        terrain = new TerrainType[0];
        price = 1;
        flex = 5;
        shape = Shape.directional;
        link = "";
    }

    public String getBoard() {
        return board;
    }

    public String getBrand() {
        return brand;
    }

    public float[] getLengths() {
        return lengths;
    }

    public float[] getWide() {
        return wide;
    }

    public String getCamber() {
        return camber;
    }

    public boolean isMale() {
        return male;
    }

    public boolean isSymmetry() {
        return symmetry;
    }

    public TerrainType[] getTerrain() {
        return terrain;
    }

    public float getPrice() {
        return price;
    }

    public int getFlex() {
        return flex;
    }

    public Shape getShape() {
        return shape;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return getBoard();
    }
}

