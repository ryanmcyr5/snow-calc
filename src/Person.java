import java.util.Comparator;

/**
 * Created by Ryan on 4/30/2016.
 */
public class Person implements Comparator<Snowboard> {

    private int skill;
    private boolean male;
    private TerrainType terrain;
    private float budget;
    private int weight;
    private int height;
    private float shoe;
    private int age;


    public Person() {
        skill = 0;
        male = true;
        terrain = null;
        budget = 0;
        weight = 0;
        height = 0;
        shoe = 0;
        age = 0;

    }

    @Override
    public int compare(Snowboard b1, Snowboard b2) {
        long l = score(b2);
        l -= score(b1);
        if (l < 0)
            return -1;
        else if (l > 0)
            return 1;
        else return 0;
    }

    public int score(Snowboard b) {
        int score = 0;
        // Skill level vs. Symmetry
        if (!b.isSymmetry() && skill < 3)
            return Integer.MIN_VALUE;
        else score += (skill - 3) * 5; // Change me
        // Sex
        if (b.isMale() == isMale())
            score += 20; // Change me
        if (isMale()) {

            if (b.getFlex() < 4) // Change me
                score += 10; // Change me
        } else {
            if (b.getFlex() > 6) // Change me
                score -= 20; // Change me
        }
        // Riding style
        boolean hasIt = false;
        for (int i = 0; i < b.getTerrain().length; i++)
            if (getTerrain() == b.getTerrain()[i]) {
                hasIt = true;
                score += (TerrainType.values().length - i) + 10; // Change me
            }

        if (!hasIt)
            return Integer.MIN_VALUE;
        // Budget
        if (getBudget() < b.getPrice())
            if (b.getPrice() - getBudget() < 20) // Change me
                score -= 5; // Change me
            else return Integer.MIN_VALUE;
        else score += (getBudget() - b.getPrice()) / 10;
        // Weight
        if (getWeight() < 150) { // Change me
            if (b.getFlex() < 6) // Change me
                score += 20;
        } else if (getWeight() < 195) {
            if (b.getFlex() >= 4 && b.getFlex() <= 8) // Change me
                score += 20; // Change me
        } else if (b.getFlex() > 5) // Change me
            score += 20; // Change me


        // Height

        // pick wide or normal based on shoe size
        // see if any heights with difference < 7
        // eliminate boards that don't fit
        // ignore back country boards
        float[] lens = getShoe() >=  10.5? b.getWide() : b.getLengths();
        hasIt = false;
        for (float len : lens)
                if (Math.abs(len - getHeight() * 7f/8) <= 7.5)
                    hasIt = true;
        if (!hasIt)
            return Integer.MIN_VALUE;
        // Age
        if (getAge() < 18 && b.getBoard().toLowerCase().contains("playboy"))
            return Integer.MIN_VALUE;

        // Camber
        if (b.getCamber().equals("roc") || b.getCamber().contains("/roc/"))
            if (getSkill() < 4) // Change me
                score += 2; // Change me

        // Shape
        if (getTerrain() == TerrainType.park && b.getShape() == Shape.twin)
            score += 2; // Change me

        return score;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public TerrainType getTerrain() {
        return terrain;
    }

    public void setTerrain(TerrainType terrain) {
        this.terrain = terrain;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getShoe() {
        return shoe;
    }

    public void setShoe(float shoe) {
        this.shoe = shoe;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
