import com.google.gson.annotations.SerializedName;

/**
 * Created by Ryan on 4/30/2016.
 */
public enum TerrainType {

    @SerializedName("1")
    park,
    @SerializedName("2")
    groomers,
    @SerializedName("3")
    backcountry;

}
